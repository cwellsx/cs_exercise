﻿# Proposal

This is quite wordy.

You said it's "not a trick question" for some reason; but I think that the requirements are a bit ambiguous or terse,
("[underspecified](https://en.wiktionary.org/wiki/underspecification)"), and I wouldn't like to implement something which
doesn't meet present requirements.

Also I suppose I'm expected to foresee (and avoid implementing) any potential deadlock, in the implementation and in the
choice of algorithm or "protocol".

## Requirements

These were per your email, I needn't quote them again here.

## Specification

First I guess that a specification is as follows, if I read between the lines of what you wrote:

- The initial state is always 3 threads and 3 colours, with 10 balls per colour and per thread
- The solution must work for *any* and every such initial state, and the data you specified was just one example
- After a request, the request exists until the peer replies, and while it exists the thread can do no more (because "only ... or")
- A send completes a request, and after a send in reply to a request, both threads (either thread) of the pair can initiate another
- A preferred solution wouldn't require "cancel", nor "timeout", nor any choosing "at random" (though a "race" may be OK)
- It's not an atomic exchange of balls, e.g. after the first send one peer will have 11 and the other peer will have 9

I interpret "A Single thread can only request or send one ball at a time to another thread" as:

- A "request" is a request for a specific colour
- A "send" is (probably) only in reply to a requested colour

### Unclear

It's not entirely clear:

1. Whether a thread's being allowed to "request or send to another thread" means
   "to two other threads (i.e. both its peers) simultaneously", or whether "another" means "only one or the other".
2. Whether a send (of a colour) is only in reply to a request (for that colour)
3. Given that a send terminates a request, and if the protocol is half-duplex,
   might a request be terminated or cancelled by the peer's replying with a counter-request instead

I will:

1. Choose the second meaning, deciding that the first is too restrictive
2. Assume this is true
3. Propose solutions which don't need this feature

## Thread identity

I'd like to assume that:

- Each thread or process has an identity or identifier
- Threads know their own identity and know the identity of each peer which they connect with
- Identities can be sorted (e.g. alphabetically)

So we can name threads, "A", "B", and "C", and code an algorithm where one of them "starts first" --
rather than their being truly equal, indistinguishable peers.

In the implementation, an "identity" could be configuration data -- e.g. its network address if it's a process,
or assigned as a constructor parameter if it's an in-process thread.

## Architecture

I'll assume three peer-to-peer connections.

In-process threads might communicate via some application-specific "centre" which acts as an intelligent router --
even if that "central router" were not an "active" component so without a thread of its own.

But equal support for an implementation using JavaScript processes implies an algorithm without an intelligent "centre" --
so instead all intelligence is distributed in the three processes --
and the middle-ware between each pair is conceptually a simple message queue or network connection,
flow-controlled by the application-level protocol to send one ball at a time.

## Too many constraints

If I assume as many constraints as possible, an illiberal view of requirements ...

- Hang after a request until the peer sends a reply, and send only in reply to a request
- After requesting and while waiting for a send, a thread can neither also request from its other peer, nor do a send to this peer

... then I think the problem is over-constrained and might have no solution -- because it might hang or deadlock with an unsatisfied request.

I see a partial solution but it requires a work-around --
i.e. after the first thread has finished requesting its preferred colour, it needs to somehow notify another thread that
it should begin to collect its --
probably not what you were expecting, not the requirement or solution you were trying to describe.

## First solution

So instead a condition you probably want to relax, a capability you'd permit, is to allow communication with two peers simultaneously:

- Each thread has two connections, to each of two peers
- On each connection -- and both simultaneously -- a thread can request or send one ball at a time

And, a solution is apparent if full-duplex -- not half-duplex -- communication is allowed on each connection, so:

- Each thread can request one ball at a time on each connection -- so two requests
- Each thread can also -- even while a request is outstanding -- send a ball on each connection in response to the request from a peer

Then with the other conditions as before, and assuming each thread has an ID, there's an algorithm (or "protocol") that would satisfy, as follows

1. Thread "A" chooses its preferred colour (e.g. "R") -- either hard-coded or preferring a colour it has more of
2. While it doesn't have 10 of that colour, thread "A" sends a request for that colour to "B"
    - If "B" has that colour then it sends it
    - If "B" doesn't have that colour then it requests it from "C", then "C" sends it to "B", and "B" relays it to "A"
3. When it acquires 10 of the colour then thread "A" can stop requesting it -- and when it has sent away any and all balls of other colours, then it can exit

While the above is happening, thread "B" does something similar:

1. Thread "B" chooses its preferred colour -- not the same colour as thread "A"s -- either hard-coded or after receiving the initial request from "A"
2. While it doesn't have 10 of that colour, thread "B" requests that colour from "C" -- which, sends it immediately, or which requests and relays it from "A"
3. Thread "B"'s requests of "C" for its preferred colour may be interspersed with its requests for "A"'s preferred colour

While the above is happening, thread "C" does something similar -- i.e. chooses the remaining colour and requests it from Thread "A", which it might relay from "B".

I haven't tested this but I think it will terminate:

- Every request can be satisfied with a corresponding send, immediately or via relay
- Each thread eventually accumulates all of its preferred colour, from its immediate neighbour or via relay
- Each thread eventually sends away all of its non-preferred colours
- Defining one directional ring -- like "request to the right, send to the left" -- prevents deadlock

This solution requires one more bit of information:

- When a thread exits then its peers must be notified of that exit
- After one thread exits, peers exchange remaining balls directly instead of via relay --
  e.g. when thread "A" exits, then thread "C" begins to request from "B" instead

## Second solution

A disadvantage of the first solution is it requires a relay of the data (which is inefficient).

An alternative solution:

- Thread "A" requests its preferred colour from "B" and from "C"
- Thread "B" requests its preferred colour from "A" and from "C"
- Thread "C" requests its preferred colour from "A" and from "B"

On receiving a request, a thread will satisfy it with a send if it can, otherwise it ignores the request

This too will terminate:

- Each preferred colour is requested and satisfied, sent, directly from both peers
- Similarly both unwanted non-preferred colours are requested by and sent away to both peers

This solution seems simpler:

- No relay
- No need for notification when a peer exits

One disadvantage is that exit is potentially messier:

- A thread issues two requests without knowing which thread will satisfy it,
  therefore one of the requests will be outstanding when another thread terminates.

Another disadvantage is that it's so simple that I wonder why you specified the initial data as you did --
makes me worry I'm missing something, misinterpreting the requirements!

This would be *a* solution though and if it's a mostly a coding exercise then the algorithm needn't be very clever.

## Summary

In practice either is feasible to implement, the second is more efficient, so unless you say otherwise I'll try to code and test the second one.

## Questions

Are the specifications correct (derived from the requirements)?

Both solutions require thread identity and asymmetric behaviour --
there's "a first thread" and it chooses it's "preferred colour" and other threads respect that choice --
this avoids "election" or "contention" or "race condition" between threads, to assign threads' preferred colours --
is that alright?

Does the solution look acceptable, matching the requirements?

And feel free to tell me anything else you'd like, about constraints or goals.

Assuming it's acceptable I'll implement my proposal in code (for the interview).
