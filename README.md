﻿## How to build

I developed this using MSVC 2015 because that's what's already installed on my machine,
and .NET Framework v4.5.2 because that was the IDE's default when I created a new Console project.

Tell me if that's inconvenient -- if you can't build and want me to use a newer version of the compiler or Framework.

## Choice of APIs

The implementation uses Thread instead of Task, because the requirements (via email) said "thread".

https://stackoverflow.com/questions/13429129/task-vs-thread-differences

## Magic literals

I use the following literals like magic numbers in the source, i.e. instead of giving them (longer) names:

- "R", "G", "B"
- "A", "B", "C"
- 3, 10

The [Proposed spec][./PROPOSAL.md] says ...

> The initial state is always 3 threads and 3 colours, with 10 balls per colour and per thread

... so that's what those are.

## Log files

Log files -- named "log_A.log", "log_B.log" "log_C.log" -- are created in the current working directory.

## Results

### Performance

Looking at the log files on my machine, it seems to take about 25 msec to get the threads started,
after which the balls are sent and the first test completes in about 5 msec --
which seems like a plausible order of magnitude.

Actually if I run it twice the whole second test is only 7 msec
apparently that 25 msec the first time was just to get warmed up.

### Simplest

The Simplest algorithm (i.e. hard-coded prefered colors) completed OK when run with the firstTest data.

However it failed when run with the TestAll data -- and the reason it fails is:

- A peer might start with 10 of its non-preferred color -- and therefore exit immediately (taking those balls with it)
- This leaves another peer, trying to request its hard-coded preference --
  but all of its preferred color disappeared, when the first peer exited

### Version1

After the failure of the Simplest algorithm I did a first Git commit of everything
(labeled "Finish testing the Simplest algorithm").

I then implement a fixed algorithm named "Version1".

This is a revised algorith, plus adding "Exit" notifications to peers when a thead exits --
which is actually only needed to handle the case where a thread exits immediately.

To make the problem a bit more challenging and real-world -- compliant with the requirements --
I don't pass data in the Exit notifications (i.e. the color which the thread has exited with).
A simple exit notifications is enough -- the remaining peers will know which balls remain.
The "simple exit notification" is analogous to a network disconnection event, rather than a network message.


