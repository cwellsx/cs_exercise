﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
 * This is all the public interfaces in one source file
 * though a usual convention is a separate source file per interface
 */

namespace CWellsX.ThreadSort
{
    /// <summary>
    /// API to request or send to a peer
    /// </summary>
    interface IRequestOrSend
    {
        /// <summary>
        /// Request a color
        /// </summary>
        /// <param name="color">The color requested</param>
        void Request(string color);

        /// <summary>
        /// Send a color
        /// </summary>
        /// <param name="color">The color sent</param>
        void Send(string color);

        /// <summary>
        /// Notify others that this thread has exited.
        /// </summary>
        void Exit();
    }

    /// <summary>
    /// An abstraction of the API provided by a thread-queue.
    /// Factory uses this interface to connect peers.
    /// </summary>
    /// <remarks>
    /// This does not extend IDisposable because there are no data members of Peer which need to be disposed -- especially
    /// https://stackoverflow.com/questions/10698107/do-we-need-to-dispose-or-terminate-a-thread-in-c-sharp-after-usage
    /// </remarks>
    interface IPeer
    {
        /// <summary>
        /// Get an interface instance which this peer has implemented to receive communication from a remote peer.
        /// </summary>
        /// <param name="remotePeerId">The identity of the remote peer for which this interface is intended</param>
        /// <returns>An interface which can be invoked by the remote thread</returns>
        /// <remarks>When a remote peer calls IRequestOrSend then it's making local procedure calls into this IPeer instance.
        /// We want this to be logially analogous to passing messages to an asynchronous network interface,
        /// so the implementation of these procedure calls is guaranteed to be non-blocking.
        /// </remarks>
        IRequestOrSend GetConnection(string remotePeerId);

        /// <summary>
        /// Connect a notification interface, which is implemented by a remote peer, to this peer.
        /// </summary>
        /// <param name="peer">The notification interface implemented by the remote peer.</param>
        /// <param name="remotePeerId">The identity of the remote peer to which this interface will communicate</param>
        void Connect(IRequestOrSend peer, string remotePeerId);

        /// <summary>
        /// Wait for this Peer's thread to exit.
        /// </summary>
        void Join();
    }

    /// <summary>
    /// An abstraction of the current data or state of a Peer.
    /// This interface to read and alter the state of a peer exists so that I can implement 'algorithm' as a separate class,
    /// i.e. so that the Peer class is algorithm-neutral and be instantiated with any of a number of algorithms.
    /// </summary>
    /// <remarks>
    /// This extends the INotified interface -- which is implemented by the peer's data and can be called by the algorithm,
    /// i.e. the algorithm does things (is active) by calling the INotified to initiate a Request or Send to a remote a peer.
    /// </remarks>
    interface IPeerState : INotified
    {
        /// <summary>
        /// The colors currently owned by this peer.
        /// </summary>
        /// <remarks>
        /// When the algorithm's OnSend notification is invoked, this collection includes the ball which was sent
        /// by the remote peer, and which this peer has just-now received.
        /// </remarks>
        IEnumerable<string> Colors { get; }

        /// <summary>
        /// Identify which peer has this state.
        /// </summary>
        string PeerId { get; }

        /// <summary>
        /// Whether we already received an Exited notification from this remote peer
        /// </summary>
        /// <param name="remotePeerId">The peer which may or may not have exited already</param>
        /// <returns></returns>
        bool HasExited(string remotePeerId);
    }

    /// <summary>
    /// Callback notifies the other peer of a pair
    /// after the other peer uses IRequestOrSend
    /// </summary>
    /// <remarks>
    /// Same API as IRequestOrSend, except an additional remotePeerId parameter, to identify which of the
    /// several IRequestOrSend instances (because we're connected to more than one remote peer).
    /// </remarks>
    interface INotified
    {
        /// <summary>
        /// Called when we receive a request from a peer, i.e. when the peer invoked its instance of the Request method
        /// </summary>
        /// <param name="remotePeerId">The peer which made the request</param>
        /// <param name="color">The requested color</param>
        void OnRequest(string remotePeerId, string color);

        /// <summary>
        /// Called when we receive a send from a peer, i.e. when the peer invoked its instance of the Send method
        /// </summary>
        /// <param name="remotePeerId">The peer which made the send</param>
        /// <param name="color">The sent color</param>
        void OnSend(string remotePeerId, string color);
    }

    /// <summary>
    /// This adds the OnExit event to the INotified interface.
    /// </summary>
    /// <remarks>
    /// The OnExit event is defined here instead of in INotified because INotified is used twice
    /// i.e. it's implemented by IPeerState as well as by the algorithm.
    /// The algorithm is notified OnExit but it doesn't decide when to exit because the exit condition --
    /// defined by the IsFinished property in each Peer -- so that's implemented by the invariant Peer code
    /// and not by the supposedly-configurable or -replaceable algorithm code --
    /// // and therefore OnExit isn't part of the IPeerState interface which can be invoked by the algorithm.
    /// </remarks>
    interface IConnected : INotified
    {
        /// <summary>
        /// Called when we receive an exit from a peer, i.e. when the peer invoked its instance of the Exit method
        /// </summary>
        /// <param name="remotePeerId">The peer which made the exit</param>
        void OnExit(string remotePeerId);
    }

    /// <summary>
    /// This is the signature for a method to create an algorithm instance.
    /// </summary>
    /// <param name="peerState">The current state of the peer as seen by the algorithm, plus
    /// methods which the algorithm uses to initiate a new Request or Send</param>
    /// <returns>An interface which the algorithm implements or exposes,
    /// and which the peer wll invoke when OnRequest or OnSend happens from a remote peer.</returns>
    delegate IConnected CreateAlgorithm(IPeerState peerState);
}
