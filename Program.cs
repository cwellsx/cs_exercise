﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CWellsX.ThreadSort
{
    class Program
    {
        static void Main(string[] args)
        {
            firstTest(Algorithms.Simplest.createAlgorithm);

            // don't enable this -- it hangs
            // TestAll.Run(Algorithms.Simplest.createAlgorithm);

            // test again with a more robust algorithm
            firstTest(Algorithms.Version1.createAlgorithm);

            // on my machine this takes about 60 seconds to run to completion
            // or 30 seconds if I disable logging in the TestAll.runTest method
            TestAll.Run(Algorithms.Version1.createAlgorithm);
        }

        static void firstTest(CreateAlgorithm createAlgorithm)
        {
            string[][] collections = new string[3][];
            collections[0] = new string[] { "R", "R", "R", "G", "G", "G", "B", "B", "B", "R" };
            collections[1] = new string[] { "G", "G", "G", "R", "R", "B", "B", "B", "R", "R" };
            collections[2] = new string[] { "B", "B", "B", "B", "R", "G", "G", "G", "G", "R" };

            ILogger[] loggers = new ILogger[] { new Logger("log_A.log"), new Logger("log_B.log"), new Logger("log_C.log") };

            Factory.RunTest(collections, createAlgorithm, loggers);
        }
    }
}
