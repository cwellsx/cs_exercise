﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CWellsX.ThreadSort
{
    static class TestAll
    {
        struct Count
        {
            public int R;
            public int G;
            public int B;

            public void Assert()
            {
                Debug.Assert(R >= 0 && G >= 0 && B >= 0 && ((R + G + B) == 10));
            }

            public string[] GetColors()
            {
                string[] result = Enumerable.Repeat("R", R).Concat(Enumerable.Repeat("G", G)).Concat(Enumerable.Repeat("B", B)).ToArray();
                return result;
            }
        }

        public static void Run(CreateAlgorithm createAlgorithm)
        {
            // it takes about 30 msec to run a test, mostly because of the time to get the threads started, apparently
            // if that mattered I could try using a thread pool or something
            // anyway let's try and test everything, and see hw long that takes.

            // This testing will assume that the sequence/order of colors within a collection doesn't matter
            // it's about 2000 tests in total

            int nTests = 0;

            Count total = new Count() { R = 10, G = 10, B = 10 };

            foreach (Count count_A in permute(total))
            {
                
                count_A.Assert();
                Count remainder = subtract(total, count_A);
                foreach (Count count_B in permute(remainder))
                {
                    count_B.Assert();
                    Count count_C = subtract(remainder, count_B);
                    count_C.Assert();
                    ++nTests;
                    // visible progress
                    if (nTests % 100 == 0)
                    {
                        Console.WriteLine(nTests);
                    }
                    runTest(createAlgorithm, count_A, count_B, count_C);
                }
            }

            Console.WriteLine(nTests);
        }

        static IEnumerable<Count> permute(Count total)
        {
            int minR = Math.Max(0, 10 - total.G - total.B);
            for (int r = minR; r <= total.R; ++r)
            {
                int minG = Math.Max(0, 10 - r - total.B);
                int maxG = Math.Min(10 - r, total.G);
                for (int g = minG; g <= maxG; ++g)
                {
                    int b = 10 - r - g;
                    Debug.Assert(b <= total.B);
                    yield return new Count() { R = r, G = g, B = b };
                }
            }
        }

        // or C# supports overloaded operators, but don't bother with that syntax now, it's not important
        static Count subtract(Count total, Count taken)
        {
            return new Count() { R = total.R - taken.R, G = total.G - taken.G, B = total.B - taken.B };
        }

        static void runTest(CreateAlgorithm createAlgorithm, Count count_A, Count count_B, Count count_C)
        {
            // this implementation is comparable to the implementation of Program.firstTest

            string[][] collections = new string[3][];
            collections[0] = count_A.GetColors();
            collections[1] = count_B.GetColors();
            collections[2] = count_C.GetColors();

            ILogger[] loggers = new ILogger[] { new Logger("log_A.log"), new Logger("log_B.log"), new Logger("log_C.log") };

            Factory.RunTest(collections, createAlgorithm, null); // passing null instead of loggers makes it run twice as fast
        }
    }
}
