﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CWellsX.ThreadSort
{
    /// <summary>
    /// This functionality isn't required, but I'd like to inspect the system's actions
    /// to verify (by inspection) that it's working as expected.
    /// This is no industrial-strength logger, and to help ensure that it won't
    /// perturb the system there's a separate logger instance per thread.
    /// </summary>
    /// <remarks>
    /// There are Debug.Assert in the code, so if it runs without triggering those then I can hope it's running correctly.
    /// This log file helps to verify that it's running at all, i.e. that it doesn't just exit without doing anything.
    /// </remarks>
    class Logger : ILogger
    {
        static bool debug = false; // beware, enabling this makes it run relatively quite slowly
        string path;
        Stopwatch stopwatch;
        List<Tuple<long, string>> lines;

        public Logger(string path, bool deletePrevious = true)
        {
            this.path = path;
            lines = new List<Tuple<long, string>>();
            if (deletePrevious && File.Exists(path))
            {
                File.Delete(path);
            }
            stopwatch = new Stopwatch();
            stopwatch.Start();
        }

        void addLine(string text)
        {
            long msec = stopwatch.ElapsedMilliseconds;
            lines.Add(new Tuple<long, string>(msec, text));
            if (debug)
            {
                Debug.WriteLine(string.Format("{0} {1} {2}", path, msec, text));
            }
        }

        public void InitialColors(string[] colors)
        {
            string text = string.Join(", ", colors);
            addLine(text);
        }

        void addEvent(string verb, string direction, string color, string remotePeerId)
        {
            addLine(string.Format("{0} {1} {2} {3}", verb, direction, color, remotePeerId));
        }

        public void OnRequest(string remotePeerId, string color)
        {
            addEvent("rqst", "->", color, remotePeerId);
        }
        public void OnSend(string remotePeerId, string color)
        {
            addEvent("send", "->", color, remotePeerId);
        }
        public void OnRequestReceived(string remotePeerId, string color)
        {
            addEvent("rqst", "<-", color, remotePeerId);
        }
        public void OnSendReceived(string remotePeerId, string color)
        {
            addEvent("send", "<-", color, remotePeerId);
        }
        public void OnExit(string remotePeerId)
        {
            addEvent("exit", "->", " ", remotePeerId);
        }
        public void OnExitReceived(string remotePeerId)
        {
            addEvent("exit", "<-", " ", remotePeerId);
        }

        // called between tests to flush to disk
        public void Flush()
        {
            Func<long, string> formatMsec = msec => msec.ToString().PadLeft(8, ' ');
            Func<Tuple<long, string>, string> getText = tuple => string.Format("{0}  {1}", formatMsec(tuple.Item1), tuple.Item2);
            IEnumerable<string> contents = lines.Select(getText);
            File.AppendAllLines(path, contents);
            lines.Clear();
        }
    }

    /// <summary>
    /// This is a do-nothing implementation of the logger,
    /// easier to use than a potentially null reference
    /// because you don't have to check for null before using it
    /// </summary>
    class NullLogger : ILogger
    {
        public void InitialColors(string[] colors) { }
        public void OnRequest(string remotePeerId, string color) { }
        public void OnRequestReceived(string remotePeerId, string color) { }
        public void OnSend(string remotePeerId, string color) { }
        public void OnSendReceived(string remotePeerId, string color) { }
        public void OnExit(string remotePeerId) { }
        public void OnExitReceived(string remotePeerId) { }
        public void Flush() { }
    }

    interface ILogger
    {
        void InitialColors(string[] colors);
        void OnRequest(string remotePeerId, string color);
        void OnSend(string remotePeerId, string color);
        void OnRequestReceived(string remotePeerId, string color);
        void OnSendReceived(string remotePeerId, string color);
        void OnExit(string remotePeerId);
        void OnExitReceived(string remotePeerId);
        void Flush();
    }
}
