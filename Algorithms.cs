﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CWellsX.ThreadSort.Algorithms
{
    /*
     * This source file could implement mre than one algorithm class
     */

    /// <summary>
    /// This is the simplest algorithm I imagined.
    /// Don't use this, it turns out to be buggy and therefore could be deleted -- see the README.md
    /// </summary>
    class Simplest : IConnected
    {
        /// <summary>
        /// This is a static factory method whose signature matches the CreateAlgorithm delegate definition.
        /// </summary>
        public static IConnected createAlgorithm(IPeerState peerState)
        {
            return new Simplest(peerState);
        }

        readonly IPeerState peerState;
        readonly string colorPreferred;
        static string[] all = new string[] { "A", "B", "C" };

        Simplest(IPeerState peerState)
        {
            this.peerState = peerState;
            colorPreferred = getPreference(peerState.PeerId);

            // alright now, issue a request to both our peers
            foreach (string remotePeerId in all.Where(peerId => peerId != peerState.PeerId))
            {
                peerState.OnRequest(remotePeerId, colorPreferred);
            }
        }

        static string getPreference(string peerId)
        {
            // assign a hard-coded preferred color to each peer
            // just use switch because it seems inconvenient to initialize a dictionary or a 2-D array using C#
            switch (peerId)
            {
                case "A": return "R";
                case "B": return "G";
                case "C": return "B";
                default: Debug.Assert(false); throw new ArgumentOutOfRangeException();
            }
        }

        public void OnRequest(string remotePeerId, string color)
        {
            // this is a request received from a remote peer --
            // send it if we have it, otherwise ignore the request (a.k.a. "drop" the request)
            // an ignored or dropped request will logically remain pending "forever"
            // but consumes no resources (except as entries in the Requests dictionaries)
            if (peerState.Colors.Any(found => found == color))
            {
                peerState.OnSend(remotePeerId, color);
            }
        }

        public void OnSend(string remotePeerId, string color)
        {
            // this is a send received from a remote peer --
            // just blindly issue the same request again, to get the next if there is one
            peerState.OnRequest(remotePeerId, colorPreferred);
        }

        public void OnExit(string remotePeerId)
        {
            // the Simplest algorithm was implemented before the Exit notification was added
            // and doesn't know what to do with it -- can't do anything very useful so does nothing.
        }
    }

    /// <summary>
    /// This is like the Simplest algorithm exept the color preferences aren't hard-coded.
    /// Instead each thread in turn selects a preference -- a preference that's chosen to
    /// not conflict with the preferences expressed by the first threads.
    /// </summary>
    class Version1 : IConnected
    {
        /// <summary>
        /// This is a static factory method whose signature matches the CreateAlgorithm delegate definition.
        /// </summary>
        public static IConnected createAlgorithm(IPeerState peerState)
        {
            return new Version1(peerState);
        }

        readonly IPeerState peerState;
        string colorPreferred;
        readonly Dictionary<string, string> remotePereference = new Dictionary<string, string>();
        static string[] allPeers = new string[] { "A", "B", "C" };
        static string[] allColors = new string[] { "R", "G", "B" };

        Version1(IPeerState peerState)
        {
            this.peerState = peerState;
            // only the first peer will set its preference now, others will later
            getColorPreferred();
        }

        public void OnExit(string remotePeerId)
        {
            // peerState has already added this to its HasExited collection
            Debug.Assert(peerState.HasExited(remotePeerId));
            // maybe now is time for another peference check
            getColorPreferred();
        }

        public void OnRequest(string remotePeerId, string color)
        {
            // now we know what this remote's preference is
            if (!remotePereference.ContainsKey(remotePeerId))
            {
                remotePereference.Add(remotePeerId, color);
                // maybe now is time for another peference check
                getColorPreferred();
            }
            else
            {
                Debug.Assert(remotePereference[remotePeerId] == color);
            }

            // this is a request received from a remote peer --
            // send it if we have it, otherwise ignore the request (a.k.a. "drop" the request)
            // an ignored or dropped request will logically remain pending "forever"
            // but consumes no resources (except as entries in the Requests dictionaries)
            if (peerState.Colors.Any(found => found == color))
            {
                peerState.OnSend(remotePeerId, color);
            }
        }

        public void OnSend(string remotePeerId, string color)
        {
            Debug.Assert(colorPreferred != null);
            // this is a send received from a remote peer --
            // just blindly issue the same request again, to get the next if there is one
            peerState.OnRequest(remotePeerId, colorPreferred);
        }

        // helper method

        void getColorPreferred()
        {
            if (colorPreferred != null)
            {
                // already done this once
                return;
            }

            // test whether previous peers have expressed their preference
            foreach (string remotePeerId in allPeers)
            {
                if (remotePeerId == peerState.PeerId)
                {
                    // ok now it's our turn to select a preference
                    break;
                }
                if (!remotePereference.ContainsKey(remotePeerId) &&
                    !peerState.HasExited(remotePeerId))
                {
                    // we don't know what this peer's preference is yet, so wait for it
                    // until we get another Request and/or Exit notification
                    return;
                }
            }

            // now we need to choose our own preference
            // choose whichever we have most of, that's not already chosen by another
            // (the following code isn't optimally efficient but we're only dealing with 3 colors)
            Dictionary<string, int> colorCounts = new Dictionary<string, int>();
            foreach (string color in allColors)
            {
                if (remotePereference.Values.Contains(color))
                {
                    // this is already chosen as the preference of a remote peer, so ignore it
                    continue;
                }
                int count = peerState.Colors.Where(found => found == color).Count();
                colorCounts.Add(color, count);
            }
            Debug.Assert(colorCounts.Count > 0);
            // see which is the biggest
            int biggestCount = colorCounts.Values.Max();
            // we might have none of our prefered, if all threads start with 5 of each,
            // so both the others have a choice, and both pick the two we have
            // Debug.Assert(biggestCount > 0);
            colorPreferred = colorCounts.Keys.First(key => colorCounts[key] == biggestCount);

            // finally now we know our color prefeerence we can request it
            foreach (string remotePeerId in allPeers.Where(peerId => peerId != peerState.PeerId))
            {
                peerState.OnRequest(remotePeerId, colorPreferred);
            }
        }
    }
}
