﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CWellsX.ThreadSort
{
    /// <summary>
    /// The requirement says that "a thread should exit" on completion
    /// which if it were a (JavaScript) "process" would mean it would destroyed completely,
    /// so this Factory does complete creation/destruction for each new test-run
    /// instead of trying to re-use Peer instances in multiple successive tests.
    /// </summary>
    static class Factory
    {
        /// <summary>
        /// Run a test using collections of coloured balls.
        /// </summary>
        /// <param name="collections">Expect three collections, each collection is an array of colors</param>
        public static void RunTest(string[][] collections, CreateAlgorithm createAlgorithm, ILogger[] loggers)
        {
            Debug.Assert(collections.Length == 3);
            if (loggers == null)
            {
                loggers = new ILogger[] { new NullLogger(), new NullLogger(), new NullLogger() };
            }
            Debug.Assert(loggers.Length == 3);

            /*
             * What we want to do is:
             * 
             * - Create the 3 peers
             * - Connect them into pairs
             * - Let them all start, but not until after they're created and connected
             * - Wait for them to finish
             */

            // Peer doesn't implement IDisposable so its instance lifetimes needn't be wrapped in "using" statement

            Peer peerA = new Peer(collections[0], "A", createAlgorithm, loggers[0]);
            Peer peerB = new Peer(collections[1], "B", createAlgorithm, loggers[1]);
            Peer peerC = new Peer(collections[2], "C", createAlgorithm, loggers[2]);

            peerA.Connect(peerB.GetConnection("A"), "B");
            peerA.Connect(peerC.GetConnection("A"), "C");

            peerB.Connect(peerA.GetConnection("B"), "A");
            peerB.Connect(peerC.GetConnection("B"), "C");

            peerC.Connect(peerA.GetConnection("C"), "A");
            peerC.Connect(peerB.GetConnection("C"), "B");

            // threads started automatically when their Connect method is called twice, simply wait for them to finish now

            peerA.Join();
            peerB.Join();
            peerC.Join();

            foreach (ILogger logger in loggers)
            {
                logger.Flush();
            }
        }
    }
}
