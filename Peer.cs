﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Diagnostics;

namespace CWellsX.ThreadSort
{
    /// <summary>
    /// This implements the "thread" with a 
    /// </summary>
    class Peer : IPeer
    {
        /*
         * The elements of this class are defined in the following sequence:
         * 
         * - nested type declarations
         * - member data
         * - the Peer constructor and the public IPeer methods
         * - private methods
         */

        /// <summary>
        /// This contains most of the peer's interesting member data -- i.e. the collection of colors --
        /// excluding the data needed to implement the multithreading (e.g. the thread and the event queue).
        /// The algorithm reads and writes this data via the IPeerData interface.
        /// All the methods of this state, though, are just simple book-keeping and assertions.
        /// </summary>
        /// <remarks>
        /// What makes this State data thread-safe is that its constuctor and connect methods are called by
        /// the Main/Factory thread before this peer's own thread is started, and afterwards this data is only
        /// read or write by this peer's own thread (which is the thread which runs the algorithm).
        /// </remarks>
        class State : IPeerState
        {
            /// <summary>
            /// This collection of the current requests are only for assertion/sanity-checking e.g. of the algorithm
            /// </summary>
            class Requests
            {
                readonly Dictionary<string, string> requested = new Dictionary<string, string>();
                public void OnRequest(string remotePeerId, string color)
                {
                    // assert there's not already a request
                    Debug.Assert(!requested.ContainsKey(remotePeerId));
                    requested.Add(remotePeerId, color);
                }
                public void OnSend(string remotePeerId, string color)
                {
                    // assert this is a response to a request
                    Debug.Assert(requested[remotePeerId] == color);
                    requested.Remove(remotePeerId);
                }
            }

            // state data

            readonly List<string> colorList; // (not named "colors" because there's a public property named "Colors")
            readonly Dictionary<string, IRequestOrSend> peers; // use these interfaces to Request from or Send to a peer
            readonly HashSet<string> exited;
            readonly Requests requestedBySelf; // for assertion/sanity-checking 
            readonly Requests requestedByPeer; // for assertion/sanity-checking 
            readonly ILogger logger;

            // methods called by the Factory thread before this Peer's thread starts

            public State(string[] colors, string id, ILogger logger)
            {
                this.PeerId = id;
                colorList = new List<string>(colors);
                peers = new Dictionary<string, IRequestOrSend>();
                exited = new HashSet<string>();
                requestedBySelf = new Requests();
                requestedByPeer = new Requests();
                this.logger = logger;
                logger.InitialColors(colors);
            }
            public void Connect(IRequestOrSend peer, string remotePeerId)
            {
                peers.Add(remotePeerId, peer);
            }

            // methods which implement IPeerState and which are only called by the algorithm

            public void OnRequest(string remotePeerId, string color)
            {
                // the algorithm wants to issue a request
                logger.OnRequest(remotePeerId, color);
                requestedBySelf.OnRequest(remotePeerId, color);
                peers[remotePeerId].Request(color);
            }
            public void OnSend(string remotePeerId, string color)
            {
                // the algorithm wants to issue a send
                logger.OnSend(remotePeerId, color);
                requestedByPeer.OnSend(remotePeerId, color);
                peers[remotePeerId].Send(color);
                bool result = colorList.Remove(color);
                Debug.Assert(result); // assert the color existed in the list
            }
            public IEnumerable<string> Colors
            {
                get { return colorList; }
            }
            public string PeerId { get; private set; }
            public bool HasExited(string remotePeerId)
            {
                return exited.Contains(remotePeerId);
            }

            // methods which are called, after an event is received from a peer, and before the algorithm is notified

            public void OnRequestReceived(string remotePeerId, string color)
            {
                logger.OnRequestReceived(remotePeerId, color);
                requestedByPeer.OnRequest(remotePeerId, color);
            }
            public void OnSendReceived(string remotePeerId, string color)
            {
                logger.OnSendReceived(remotePeerId, color);
                requestedBySelf.OnSend(remotePeerId, color);
                colorList.Add(color);
            }
            public void OnExitReceived(string remotePeerId)
            {
                logger.OnExitReceived(remotePeerId);
                bool result = exited.Add(remotePeerId);
                Debug.Assert(result);
            }

            // property which determines whether the thread's exit condition is true

            public bool IsFinished
            {
                get { return colorList.Count == 10 && colorList.All(color => color == colorList[0]); }
            }

            // property which determines whether this Peer has all its connections to its peers
            // (though the peers might not yet have their connections back to this)

            public bool IsConnected
            {
                // 3 Peer instances in total, minus 1 which is this Peer
                get { return peers.Count == (3 - 1); }
            }

            // method called by the thread when IsFinished returns true

            public void Exit()
            {
                foreach (string remotePeerId in peers.Keys)
                {
                    logger.OnExit(remotePeerId);
                    peers[remotePeerId].Exit();
                }
            }
        }

        /// <summary>
        /// Our GetConnection method instantiates this, to create IRequestOrSend interface instances,
        /// which the Factory then passes as a parameter to a remote peer's Connect method.
        /// The IRequestOrSend methods of this class are called by the remote peer's thread, and this implements
        /// those methods, by creating actions which are enqueued and subsequently run by this Peer's thread.
        /// </summary>
        class Callback : IRequestOrSend
        {
            readonly Peer self;
            readonly string remotePeerId;

            public Callback(Peer self, string remotePeerId)
            {
                this.self = self;
                this.remotePeerId = remotePeerId;
            }

            // thread-safely enqueue the event as an Action to be run by the thread

            public void Request(string color)
            {
                Action action = () => self.onRequest(remotePeerId, color);
                self.enqueueAction(action);
            }
            public void Send(string color)
            {
                Action action = () => self.onSend(remotePeerId, color);
                self.enqueueAction(action);
            }
            public void Exit()
            {
                Action action = () => self.onExit(remotePeerId);
                self.enqueueAction(action);
            }
        }

        // this is all the Peer member data

        readonly State state;
        readonly CreateAlgorithm createAlgorithm;
        readonly Thread thread;
        // I use Monitor to make this queue thread-safe e.g. as described at https://jonskeet.uk/csharp/threads/deadlocks.html#monitor.methods
        readonly object queueLock = new object();
        readonly Queue<Action> queued; // this is not one of the thread-safe collection classes but it is protected using queueLock
        IConnected algorithm; // could create this immediately but let's delay that until the thread starts ater the state IsConnected

        // constructor, and other methods, which are invoked only by the Factory

        public Peer(string[] colors, string id, CreateAlgorithm createAlgorithm, ILogger logger)
        {
            state = new State(colors, id, logger);
            this.createAlgorithm = createAlgorithm;
            // ThreadStart is a delegate in .NET and can be an instance method and doesn't have to be static
            thread = new Thread(this.threadStart);
            queued = new Queue<Action>();
            // wait until IsConnected before calling thread.Start
        }
        public IRequestOrSend GetConnection(string remotePeerId)
        {
            return new Callback(this, remotePeerId);
        }
        public void Connect(IRequestOrSend peer, string remotePeerId)
        {
            state.Connect(peer, remotePeerId);
            if (state.IsConnected)
            {
                // we can now initiate a Request or Send to both peers, so now start the thread,
                // which creates the algorithm and then services queued actions --
                // by the way we might now already have enqueued actions,
                // received via our Callback before both our connections were installed
                thread.Start();
            }
        }
        public void Join()
        {
            thread.Join();
        }

        // the thread procedure

        void threadStart()
        {
            algorithm = createAlgorithm(state);
            // this exits immediately if the initial state is already IsFinished
            // if the API or Algorithm is changed to depend on an Exit notification being propagated to other peers, then this would be changed
            while (!state.IsFinished)
            {
                // wait for any action to be enqueued
                Action action = dequeueAction();
                // run the action, which will be either this.onRequest or this.onSend
                action();
            }
            // notify our peers that we're exiting
            state.Exit();
        }

        // methods for a thread-safe producer/consumer queue
        // enqueue is called from the Callback methods, and dequeue is called by the thread procedure

        Action dequeueAction()
        {
            lock (queueLock)
            {
                if (queued.Count != 0)
                {
                    // this return will release the lock
                    // so the lock isn't held while the thread executes the action
                    // so enqueueAction -- called by another thread -- won't ever block for long nor deadlock
                    return queued.Dequeue();
                }
                // queue is empty so wait
                // this implicitly and temporarily releases the queueLock
                Monitor.Wait(queueLock);
                // the Wait completes when Monitor.Pulse is called
                // this implicitly reacquires the queueLock
                Debug.Assert(queued.Count != 0);
                return queued.Dequeue();
            }
        }

        void enqueueAction(Action action)
        {
            lock (queueLock)
            {
                queued.Enqueue(action);
                Monitor.Pulse(queueLock);
            }
        }

        // these methods are what the Callback methods enqueue as Action instances
        // and which are dequeued and invoked by the thread procedure.
        // they simply update the state and then invoke the algorithm.

        void onRequest(string remotePeerId, string color)
        {
            state.OnRequestReceived(remotePeerId, color);
            algorithm.OnRequest(remotePeerId, color);
        }
        void onSend(string remotePeerId, string color)
        {
            state.OnSendReceived(remotePeerId, color);
            algorithm.OnSend(remotePeerId, color);
        }
        void onExit(string remotePeerId)
        {
            state.OnExitReceived(remotePeerId);
            algorithm.OnExit(remotePeerId);
        }
    }
}
